﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Number : MonoBehaviour
{
    public static int number;
    private bool correct;
    public static int numCorrect;
    private bool redGreen;
    public static int numRedGreen;
    private bool protan;
    public static int numProtan;
    private bool deutran;
    public static int numDeutran;
    public static int numTotal;
    private Answers question;

    // Start is called before the first frame update
    public void pressButton(string button)
    {
        question = GameObject.Find(button).GetComponentInChildren<Answers>();
        correct = question.correct[number];
        redGreen = question.redGreen[number];
        protan = question.protan[number];
        deutran = question.deutran[number];
        if(correct == true)
        {
            numCorrect++;
        } else if (redGreen == true)
        {
            numRedGreen++;
        } else if (protan == true)
        {
            numProtan++;
        }else if (deutran == true)
        {
            numDeutran++;
        }
        else
        {
            numTotal++;
        }
        correct = false;
        redGreen = false;
        protan = false;
        deutran = false;
        number++;
        if (number == 14)
        {
            
            loadScreen("resultats");
        }

    }

    public void loadScreen(string sceneName)
    {
        SceneManager.LoadScene(sceneName);
    }
}
