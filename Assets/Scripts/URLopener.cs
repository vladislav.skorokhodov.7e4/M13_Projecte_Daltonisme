﻿using UnityEngine;

public class URLopener : MonoBehaviour
{
    public string URL;

    public void Open()
    {
        Application.OpenURL(URL);
    }

}
