﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Results : MonoBehaviour
{
    public string[] resultats;
    void Start()
    {
        string[] changeText = GameObject.Find("Canvas").GetComponentInChildren<Results>().resultats;
        GameObject GO = GameObject.Find("Numbers");
        int correct = Number.numCorrect;
        int redGreen = Number.numRedGreen;
        int total = Number.numTotal;
        int deutran = Number.numDeutran;
        int protan = Number.numProtan;
        Number.numCorrect = 0;
        Number.numRedGreen = 0;
        Number.numTotal = 0;
        Number.numDeutran = 0;
        Number.numProtan = 0;
        Number.number = 0;
        if (deutran>=2){
            GameObject.Find("Explicació").GetComponentInChildren<Text>().text = resultats[2];
        }
        else if  (protan >= 2){
            GameObject.Find("Explicació").GetComponentInChildren<Text>().text = resultats[3];

        }
        else if (redGreen >= 3)
        {
            GameObject.Find("Explicació").GetComponentInChildren<Text>().text = resultats[1];

        }
        else if (correct >= 14)
        {
            GameObject.Find("Explicació").GetComponentInChildren<Text>().text = resultats[0];

        }
        else if (total >= 6)
        {
            GameObject.Find("Explicació").GetComponentInChildren<Text>().text = resultats[1];
        }
    }
}
